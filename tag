#!/bin/bash

function pushTag() {

  read -e -p "Tag für einen deploy in repo pushen? y or n: " -N 1 answer

  case $answer in
    y|Y|j|J)
      git push --tags
      ;;

    *)
      exit 0;
      ;;
  esac
}

NAME=`date +%FT%H%M`
echo Erstelle Tag ${NAME}

# -s erstellt eine signatur, damit wird der tag automatisch zu einem annotated tag
# es muss eine Nachricht mit angegeben werden.
git tag -s ${NAME}
pushTag
