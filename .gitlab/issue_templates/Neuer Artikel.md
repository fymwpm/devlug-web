Neuen Artikel für die Homepage.

* Artikel: <Titel des Artikels>
* URL: <URL>

# Beschreibung
## Kurzbeschreibung
(Hier eine Kurzbeschreibung)
## Zielgruppe
(Welche Zielgruppe hat der Artikel)
### Inhalsverzeichnis
(Idee für Inhaltsverzeichnis oder Strichworte)

/label ~Artikel

/milestone %"devLUG-Sprint 2018-06"

