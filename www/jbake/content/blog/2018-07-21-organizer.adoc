= organizer 
DebXWoody
2018-07-21
:jbake-type: post
:jbake-status: published
:jbake-tags: organizer, cpp, dev
:idprefix:
[abstract]
Seit einiger Zeit bin ich auf dem Trip "5 Füllfederhalter, Leuchtturm
A5 dotted und Bullet Journal", nachdem ich feststellen musste, dass
der Filofax einfach für meine Bedürfnisse nicht ganz so passend ist.
Die 5 verschiedenen Farben aus den Füllfederhaltern machen auch
einiges her, wenn man sich die Notizen auf den dotted Papier ansieht.
Bei der Sache gibt es nur ein Problem,...  ich bin ein Informatiker!
Die ganzen Sachen kann man doch auch auf dem PC verwalten.
Also mal Spaß beiseite,...

== Warum eine Organizer-Anwendung?
Ich nutzte neomutt, remind + wyrd, abook und task + timew. Vielleicht
muss ich noch mal genauer überlegen was eigentlich mein Problem bei
der Sache ist oder ob ich einfach nur Lust auf eine
Softwareentwicklung für Linux in C++ habe. Irgendwie, irgendwas fehlt
mir oder stört mich einfach. Wobei viele der oben gennaten Anwendungen
eigentlich sehr cool sind. Ich bin kein Freund von "Cloud"-Anwendungen
und verwende gerne Terminal-Anwendungen. Alles muss ich nicht auf dem
Handy haben und alle Daten müssen auch nicht immer über das Internet
gehen.

== Das Problem
Besonders bei Aufgaben, Termine, Kalender, Kontakten und Erinnerungen
stehen die Informationen sehr stark in Beziehung. Kontakte haben
vielleicht einen Geburtstag, welche im Kalender angezeigt werden soll.
Aus einer Erinnerung wird vielleicht eine Aufgabe.

Viele dieser Konzepte findet man in einigen Programmen wieder. Egal ob
Linux, OSX oder diesem Nachfolger von DOS. Aber irgendwie sind es
nicht die Programme die ich verwenden will und Terminal Anwendungen
gibt es da selten.

Wichtig ist jedoch, dass die Konzepte eigentlich sehr ähnlich sind und
die Daten für einen Benutzer auch eigentlich immer gleich sind. Meine
Kontakte ändern sich nicht, nur weil ich von claws-mail auf neomutt
gewechselt habe. Meine Termine bleiben gleich, ob ich mir diese mit
remind oder in einer X-Anwendung ansehe.

Viele Anwenundgen haben keine Trennung zwischen UI und Modell / Logik.

== Die Organizer Anwendung
Ich möchte eine Anwendung entwickeln die eine klare Trennung von UI
und Modell hat. In denen die Kontakte, Termine etc. in einer Shell
oder in X verwenden werden können und beliebige Anwendungen die
Möglichkeit haben die Daten zu nutzen, ohne diese ins WWW zu schieben.

Ein Prototyp ist hier: https://gitlab.com/devlug/devel/organizer/organizer-prototyp

Wer Interesse hat mitzumachen, einfach melden.


