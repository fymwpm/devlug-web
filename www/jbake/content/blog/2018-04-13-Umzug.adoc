= Wir ziehen um 
Stefan Kropp
2018-04-13
:jbake-type: post
:jbake-status: published
:jbake-tags: vlughessen, git
:idprefix:
== Wir ziehen von github nach gitlab
Wir werden dieses Wochenende unsere Git Repositories von GitHub nach GitLab umziehen.
Ihr findet uns jetzt auf https://gitlab.com/vlug-hessen

=== Update: So, 2018-04-15 14:00 
Wir sind erfolgreich umgezogen. Die Repositories sind jetzt auf GitLab.com.



