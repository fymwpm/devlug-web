= #devLUG #13
DebXWoody
2018-08-21
:jbake-type: post
:jbake-status: published 
:jbake-tags: #devlug
:idprefix:
[abstract]
Die #devlug #13 war am 21.08.2018 im IRC.

== TOP 1 - Uhrzeit
Wir haben den Stammtisch auf 20:30 Uhr verschoben. Nur noch
mal als Erinnerung. 

== TOP 2 - devTuxBot
Unser Bot devTuxBot werden wir jetzt nach und nach ausbauen.
Alle die Interesse haben, können im Raum #devlug-bot vorbei
schauen und sich eine eggdrop zum testen und programmieren
in den channel hängen. Infos bekommt man im Channel via
!tux.

== TOP 3 - offizielle Gruppe im freenode
Hatte ich ja schon auf der Mailingliste angesprochen.
Wir haben noch einen weiteren Raum #devlug-offtopic wenn man
sich auch über Dinge unterhalten will die nichts mit Linux /
devlug etc zu tun haben.

== TOP 4 - devLUG-Session
Vorschlag für die nächste Session hätte ich das Thema "git".

== TOP 5 - devLUG
Alternativer Name für Stammtisch ist "#devlug".

