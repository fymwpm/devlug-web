= Admin- und Orga-Team 
DebXWoody
2018-05-27
:jbake-type: devlug
:jbake-status: published
:jbake-tags: devlug 
:jbake-updated: 2018-06-10
:idprefix:
:toc:
[abstract]
Informationen zum Admin- und Orga-Team der devLUG.

== devlug-admin
Das Admin- und Orga-Team besteht im Moment aus:

* https://gitlab.com/ancho[Frank] 
* https://gitlab.com/DebXWoody[Stefan]

Uns erreichst du unter  kontakt@devlug.de.


