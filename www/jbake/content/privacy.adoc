= Datenschutzerklärung 
DebXWoody
2018-07-21
:jbake-type: page
:jbake-status: published 
:jbake-tags: 
:idprefix:
== Allgemeine Informationen
Die devLUG ist eine Gruppe von Personen welche über Dienste im Internet
kommunizieren (Stichwort: virtueller Stammtisch).

Wir nutzen eigene Dienste und externe Dienste von Drittanbieter.

== Verarbeitung von personenbezogenen Daten
=== Mailinglisten
Die Mailinglisten der devLUG `lists.devlug.de` werden auf einem Server-Account
bei uberspace verarbeitet.
Informationen zum Datenschutz findest du hier: https://uberspace.de/privacy

Wenn du eine Mailingliste abonnierst, können die folgenden Daten für die Eintragung gespeichert
werden. Informationen aus vom FROM deiner E-Mail für die Registrierung. Diese beinhaltet deine E-Mail-Adresse
und kann deinen Namen beinhalten `Vornamen Nachname <user@domain.tld>`. Diese Informationen sind nötig, um den Betrieb 
der Mailingliste sicherzustellen. Die Eintragung ist freiwillig und wird von der Person vorgenommen. Der Benutzer
kann sich jeder Zeit wieder abmelden.

Auch wenn wir versuchen die personenbezogenen Daten zu Schützten, so sehen wir die Mailingliste
als eine `Öffentliche E-Mail Verteiler`. Jede Person kann sich auf diesen Mail-Verteiler eintragen
und die Kommunikation auf der Mailingliste verfolgen. Eine Bereitstellung von einem Archiv der
Mailingliste kann ggf. bereitgestellt werden.

== Repositories
Unserer Repositories liegen auf gitlab. Die Mitarbeiter in diesen Repositories ist freiwillig
und liegt in der Verantwortung des Nutzers. 
Informationen zum Datenschutz über gitlab.com sind hier: https://about.gitlab.com/privacy/	

== Homepage
Auf unserer Homepage werden nur `Nickname` verwendet, die jede Person selber aussuchen kann.
Wir möchten auf unserer Homepage weder Namen, E-Mail-Adresse noch URL einer Homepage unserer
Mitglieder haben. 

== Links
Wir übernehmen keine Verantwortung von Links auf Webseiten oder Dienste außerhalb von www.devlug.de.

== Soziale Netzwerke und Tracking- und Analysetools
Wir verwenden keine Tracking-, Analyse,- und Sozale Netzwerk-Software auf unserer Homepage ein.

== Mastodon
Die devLUG hat einen Account auf der Mastodone Instanz social.tchncs.de. Informationen: https://social.tchncs.de/about/more

== Chat
Zur Kommunikation verwenden wir das IRC-Netzwerk auf freenode.
Die Kommunikation in diesem Netzwerk ist freiwillig und liegt in der Verantwortung 
des Benutzers. Informationen zum Datenschutz sind hier: https://freenode.net/policies

 
